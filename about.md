# Pourquoi refaire une carte des ressources ?

Les cartes de Dofus ne manquent pas sur le net. Mais le constat est simple : 

  * incomplètes (certaines ressources manquantes ou idées laissées de côté)
  * mal renseignées, pas mises à jour (normal, faut du monde)
  * lentes et lourdes (technologies utilisées pas adaptées)

Ce que nous proposons ici n'est pas de résoudre tout les problèmes ni de concurrencer les autres cartes, mais d'avoir une vision différente !

Ainsi nous nous mettons comme but de : 

  * proposer plusieurs cartes (Incarnam par exemple)
  * proposer les ressources connues (chanvre, lin, cuivre, bambou, etc.)
  * d'ajouter d'autres ressources moins connues (poubelles qui contiennent de tout, coffres dans les zones Frigost, etc.)
  * de mettre à jour fréquemment (joignez-vous à nous, lisez la section **Se joindre à l'aventure**)
  * d'être plus rapide que les autres Cartes de Dofus en utilisant des technologies plus adaptées et moins lourdes
  * de rendre plus facile le partage de l'adresse du site pour une ressource donnée (pour partager avec les amis une ressource)
  * de permettre une participation facile sans s'y connaître en informatique

Le fruit de notre travail est donc celui que vous lisez actuellement sur Lighty Dmap.

# Se joindre à l'aventure !

Vous êtes un baroudeur chevroné ou bien un simple aventurier qui s'amuse à Astrub ? Pas de problème, joignez-vous à nous pour localiser les ressources de tout type sur le continent et plus encore !

## Le contenu à apporter : La théorie

Le défi est simple : donner pour **chaque case** de la carte, les ressources disponibles et leur quantité. Avouez qu'on a fait pire dans le genre compliqué, non ?

Alors si cela vous convient, passons à l'application pratique de tout ça !

## La pratique

On se balade tranquille, dans Amakna, quand tout à coup, sans crier gare, la carte de coordonnée *1,-8* apparaît ! Quelle chance ! On voit des ressources par là.

Donc je note tout de suite ce que je vois :

<pre>
Carte: 1,-8
[edelweiss]
qte = 3

[menthe]
qte = 1

[orchidee]
qte = 1
</pre>

Et voilà, je prépare ça dans un fichier texte (Notepad ou Gedit)  pour l'envoyer à Lighty Dmap afin qu'ils s'occupent de l'intégrer à la prochaine version de la carte de ressources.

## J'envoie comment et à qui ?

On va faire simple : vous jouez à Dofus, Ankama propose un système de messagerie nommé **Ankabox**, utilisons-le !

  * Connectez vous à [Ankabox](https://ankabox.ankama.com/fr/ "Se rendre sur le site d'Ankabox")
  * Écrivez à **Tod6784** ou **Pappou**
  * Donnez le contenu précédent pour chaque carte de ressources
  * Si jamais vous avez été sur une carte qui ne contient pas de ressources, vous pouvez aussi l'indiquer ! Savoir qu'elle ne contient rien nous est utile pour compléter au mieux la carte totale !

## Quel codes utiliser pour les ressources ?

Voici une liste des codes ressources utilisés. D'autres codes viendront bientôt pour des ressources particulières (comme les coffres, les poubelles, etc.).

### Ressources de base

  * lin
  * chanvre
  * ortie
  * sauge
  * trefles
  * menthe
  * orchidee
  * edelweiss
  * pandouille
  * perce-neige
  * ginseng
  * belladone
  * mandragore
  * ble
  * orge
  * avoine
  * houblon
  * seigle
  * riz
  * malt
  * frostiz
  * mais
  * millet
  * fer
  * cuivre
  * bronze
  * kobalte
  * manganese
  * etain
  * argent
  * silicate
  * bauxite
  * or
  * dolomite
  * obsidienne
  * frene
  * chataignier
  * noyer
  * chene
  * oliviolet
  * bombu
  * erable
  * if
  * bambou
  * merisier
  * ebene
  * noisetier
  * kalyptus
  * charme
  * bambou_sombre
  * orme
  * bambou_sacre
  * tremble
  * goujon
  * greuvette
  * truite
  * crabe
  * chaton
  * pane
  * carpe
  * sardine
  * brochet
  * kralamoure
  * anguille
  * dorade
  * perche
  * raie
  * lotte
  * requin
  * bar
  * morue
  * tanche
  * espadon
  * poisskaille

### Divers

  * poubelle
  * puit
  * zaap
  * zaapi
  * banque
  * rp (salles secrètes ou non, pour faire du Jeu de Rôle ou être tranquille pour effectuer des tortures diverses sur des Wabbits)
  * cadeau (uniquement dans l'île de Nowel)
  * atelier (ateliers cachés, etc.)
  * phenix (statue phénix)
  * egouts (entrée vers les égouts)

# Remerciements

Ce site web existe grâce à l'intervention de plusieurs personnes que nous tenons à remercier.

## Les créateurs

  * Pappou (Jiva)
  * Orophin (Bowisse)

Si vous êtes contents du travail effectué, n'hésitez pas à nous payer une bière dans le jeu si vous nous voyez ou bien à nous donner quelques kamas ;).

## Développeurs

Ils ont participé à l'élaboration/maintenance des logiciels du projet : 

  * Khildine (Bowisse)

## Participants

  * Keeani (Bowisse)
  * Cylisa (Bowisse)
  * Elixies (Bowisse)
  * Ete (Bowisse)
  * Flipflapflop (Bowisse)
  * Doutchoo (Bowisse)
  * Pepe-Henri (Bowisse)
  * Supranunu (Bowisse)
  * Fruity-Head (Bowisse)
  * Eomye (Bowisse)
  * Polluxus (Maimane)
