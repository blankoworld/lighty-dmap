# Fabuleuse aventure de cartographie

Lighty Dmap, c'est le recensement des ressources et objets considérés comme utiles par les membres de la guilde Fabuleux sur le serveur Bowisse.

C'est donc le résultat d'un travail de plusieurs personnes qui suivent les règles de participation à la cartographie de Dofus (Cf. la section **À propos** du menu en haut à gauche). Nous ne les remercierons jamais assez pour cela.

# Comment utiliser la carte ?

**En premier lieu choisissez une carte qui vous intéresse**, par exemple Amakna ou Incarnam.

Ensuite, voici quelques éléments intéressants au sujet de la cartographie.

## Le menu en haut à gauche

En haut à gauche se trouve un carré de couleur bleu vous permettant d'afficher un menu. Ce dernier fonctionne de manière particulière : 

  * la page d'accueil d'une carte contient **toutes les ressources recensées**
  * vous pouvez afficher seulement les ressources d'un métier particulier en cliquant sur le **nom du métier**
  * pour avoir le détail des ressources d'un métier ou d'une catégorie, cliquez sur la **coche +** située à gauche de la catégorie ou du métier qui vous intéresse
  * après avoir listé l'ensemble des ressources d'un métier, cliquez sur la ressource pour afficher une carte qui liste lesdites ressources
  * vous pouvez constater que l'adresse réticulaire (URL) est facile à mémoriser et à partager, par exemple pour partager avec vos amis les positions du frêne, donnez leur ceci : [http://dmap.depotoi.re/amakna/frene.html](http://dmap.depotoi.re/amakna/frene.html "Connaître l'ensemble des ressources de frêne recensées par Lighty Dmap")

## Le panneau latéral droit

Une barre verticale permet d'afficher le panneau latéral. Il contient les fonctionnalités suivantes : 

  * la plupart des éléments servent de légende à la carte
  * il donne un critère visuel quant à l'opacité d'une case qui indique à peu près le nombre de ressources connues sur une coordonnée donnée
  * il permet de zoomer sur la carte
  * on peut enlever/ajouter les coordonnées sur la carte. Notez qu'à un certain niveau de zoom, les cases sont trop petites pour afficher une coordonnée.

On apprend ainsi que : 

  * un trèfle à côté d'une ressource indique que la ressource a été vérifiée par le staff. Elle est donc réellement présente. Les autres ressources sont donc des contributions de personnes extérieures au staff.
  * une croix à côté d'une ressource indique que la plupart d'entre elles sont cachées sur la zone. Soit à un autre niveau de la coordonnée, soit derrière d'autres ressources, un muret, une salle cachée, etc.

## Le contenu de la carte

Allez sur une case pour en afficher le contenu. C'est aussi simple que ça !
