#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script aims to migrate Lighty Dmap frags to a Django application (next
# version)

# API access
server = 'localhost:8000/api'
user = 'olivier'
password = 'olivier'

# Default values
default_directory = u'test/frags'
extension = '.blk'

# ressources
authorized_resources = {
    'anguille': "Anguille",
    'aquajou': "Aquajou",
    'argent': "Argent",
    'atelier': "Atelier",
    'avoine': "Avoine",
    'bambou': "Bambou",
    'bambou_sacre': "Bambou Sacré",
    'bambou_sombre': "Bambou Sombre",
    'banque': "Banque",
    'bar': "Bar Rikain",
    'bauxite': "Bauxite",
    'belladone': "Belladone",
    'ble': "Blé",
    'bombu': "Bombu",
    'brochet': "Brochet",
    'bronze': "Bronze",
    'cadeau': "Paquet Cadeau",
    'carpe': "Carpe d'Iem",
    'chanvre': "Chanvre",
    'charme': "Charme",
    'chataignier': "Châtaignier",
    'chaton': "Poisson-Chaton",
    'chene': "Chêne",
    'crabe': "Crabe Sourimi",
    'cuivre': "Cuivre",
    'dolomite': "Dolomite",
    'dorade': "Dorade Grise",
    'ebene': "Ébène",
    'ecume': "Écume de mer",
    'edelweiss': "Edelweiss",
    'egouts': "Entrée vers les égouts",
    'erable': "Érable",
    'espadon': "Espadon",
    'etain': "Étain",
    'fer': "Fer",
    'frene': "Frêne",
    'frostiz': "Frostiz",
    'ginseng': "Ginseng",
    'goujon': "Goujon",
    'greuvette': "Greuvette",
    'houblon': "Houblon",
    'if': "If",
    'kaliptus': "Kaliptus",
    'kobalte': "Kobalte",
    'kralamoure': "Kralamoure",
    'lin': "Lin",
    'lotte': "Lotte",
    'mais': "Maïs",
    'malt': "Malt",
    'mandragore': "Mandragore",
    'manganese': "Manganèse",
    'menthe': "Menthe Sauvage",
    'merisier': "Merisier",
    'millet': "Millet",
    'mine': "Entrée de mine",
    'morue': "Morue",
    'noisetier': "Noisetier",
    'noyer': "Noyer",
    'obsidienne': "Obsidienne",
    'oliviolet': "Oliviolet",
    'or': "Or",
    'orchidee': "Orchidée Sauvage",
    'orge': "Orge",
    'orme': "Orme",
    'ortie': "Ortie",
    'pandouille': "Pandouille",
    'pane': "Poisson Pané",
    'patelle': "Patelle",
    'perce-neige': "Perce-Neige",
    'perche': "Perche",
    'phenix': "Statue Phénix",
    'poisskaille': "Poisskaille",
    'poubelle': "Poubelle",
    'puit': "Puit",
    'quisnoa': "Quisnoa",
    'raie': "Raie Bleue",
    'requin': "Requin Marteau-Faucille",
    'riz': "Riz",
    'rp': "Pièces pour JdR",
    'salikrone': "Salikrone",
    'sardine': "Sardine Brillante",
    'sauge': "Sauge",
    'seigle': "Seigle",
    'silicate': "Silicate",
    'tanche': "Tanche",
    'trefles': "Trèfles",
    'tremble': "Tremble",
    'truite': "Truite",
    'zaap': "Zaap",
    'zaapi': "Zaapi",
}


import sys

from os.path import exists
from os.path import isdir
from os import listdir

import re

import requests

from configparser import ConfigParser
from configparser import DuplicateSectionError

# Regular expression for the filename (resources)
#+ Is composed of:
#+  - a minus char that appears 0 or 1
#+  - some digit that appear 1 or 3 digits
#+  - a comma (mandatory)
#+  - a minus char (as previously, 0 or 1 time)
#+  - some digit that appear 1 or 3 digits
#+  - the end (so more than 3 digits is not allowed
regex_filename = re.compile('(-?\d{1,3},-?\d{1,3})%s$' % extension)


def check_api(server, user, password):
    """
    Check that we are authorized to access API
    """
    url = 'http://%s/ressources/' % server
    r = requests.get(url, auth=(user, password))
    if r.status_code != 200:
        raise Exception(
            'API not accessible. URL: %s. User: %s. Password: %s' % (
                url, user, password))
    return True

def check_directory(dirname):
    """
    Check that directory exists.
    That some directory are into. Each directory is a MAP.
    And that some file are into each MAP.
    """
    # Some checks
    if not dirname:
        raise Exception("Please give a directory.")
        return False
    if not exists(dirname):
        raise Exception("This directory doesn't exist: %s" % dirname)
    # Prepare some values
    maps = {}
    useful_files = False
    for element in listdir(dirname):
        element_path = '/'.join([dirname, element])
        if not isdir(element_path):
            print("[EXCLUDED] Not a directory: %s" % element_path)
            continue
        files = listdir(element_path)
        if not files:
            print("[EXCLUDED] No file found: %s" % element_path)
            continue
        maps.update({element.lower(): files})
        for f in files:
            # Check that there is at least 1 file that is readable
            if f.endswith(extension):
                useful_files = True
                break
    if not useful_files:
        raise Exception("No %s files found!" % extension)
    return maps


def check_file(name):
    # Do not process files that have not the right extension
    if not name.endswith(extension):
        print("[EXCLUDED]  %s (wrong extension)" % name)
        return False
    # Do not process malformed files
    if not regex_filename.search(name):
        print("[MALFORMED] %s (wrong name)" % name)
        return False
    print("Processing %s…" % name)
    match = regex_filename.match(name)
    coordinate = match.groups()[0]
    return True


def get_ressources(url):
    res = {}
    r = requests.get(url)
    if r.status_code != 200:
        raise Exception('Failed to fetch ressources from API.')
    content = r.json()
    for ressource in content:
        ressource_id = ressource.get('id')
        code = ressource.get('code')
        res[code] = ressource_id
    return res


def get_amakna_map_id(url, usr, pwd):
    res = None
    r = requests.get(url, auth=(usr, pwd))
    if r.status_code != 200:
        raise Exception('Failed to fetch cartes from API.')
    content = r.json()
    for carte in content:
        if carte.get('nom') == 'Amakna':
            res = carte.get('id', None)
    return res


def get_or_create_coordinate_url(url, coordinate, map_url):
    res = ''
    r = requests.get(url, params={'coordonnee': coordinate})
    r.raise_for_status()
    if r.json() == []:
        # create localisation
        r = requests.post(
            url,
            auth=(user, password),
            data={'coordonnee': coordinate, 'carte': map_url})
        r.raise_for_status()
        content = r.json()
        res = content.get('id')
    else:
        content = r.json()
        res = content[0].get('id')

    return '%s%s/' % (url, res)

def read_frag(filepath):
    """
    Read file and extract only authorized resources.
    """
    # Prepare some values
    res = {}
    # Read file
    conf = ConfigParser()
    try:
        conf.read(filepath)
    except DuplicateSectionError as e:
        print('Duplicate: %s' % filepath)
        return res
    for section in conf.sections():
        section_name = section.lower()
        # Do not process non authorized resources
        if section_name not in authorized_resources.keys():
            print("\t%s: unknown" % section_name)
            continue
        if section not in res:
            res[section_name] = {}
        # By default consider that resources were not checked/verified (safe = False)
        res[section_name].update({
            'safe': False,
        })
        for key, value in conf.items(section):
            # Make safe as a boolean
            if key == 'safe' and value != 0:
                value = True
            # Make qte to be an integer
            if key == 'qte':
                value = int(value)
            # Make hide as integer only if superior to 0
            if key == 'hide' and int(value) > 0:
                value = int(value)
            # write result
            res[section_name].update({
                key: value,
            })
    return res


def process(frag, coordinate_url, ressources, url_inventaire, url_ressource):
    """
    Check if this inventory line exists on API.
    If not, create a new one.
    """
    for ressource_code in frag:
        ressource_id = ressources.get(ressource_code, None)
        if not ressource_id:
            # TODO: create new ressource
            print('\t%s not found!' % ressource_code)
            continue
        parameters = frag[ressource_code]
        qte = parameters.get('qte', None)
        hide = parameters.get('hide', None)

        detail = {
            "localisation": coordinate_url,
            "ressource": '%s%s/' % (url_ressource, ressource_id),
            "dofus_version": '2.34',
        }
        if qte:
            detail.update({"nombre_total": int(qte)})
        if hide:
            detail.update({"nombre_cachee": int(hide)})

        write_ressource = requests.post(
            url_inventaire,
            auth=(user, password),
            data=detail)
        try:
            write_ressource.raise_for_status()
        except:
            print(write_ressource.json(), detail)
    return True


def main(args=False):
    """
    For each map directory, read frags and send them to the API.
    """
    # Check API is available
    check_api(server, user, password)

    # Check source directory
    directory = (args and len(args) > 1 and args[1]) or default_directory
    maps = check_directory(directory)
    if not maps:
        raise Exception("No maps found into %s" % directory)

    # Fetch all resources from API and make a dict containing needed elements
    url_ressources = 'http://%s/ressources/' % server
    ressources = get_ressources(url_ressources)
    url_map = 'http://%s/cartes/' % server
    map_id = get_amakna_map_id(url_map, user, password)
    url_inventaire = 'http://%s/inventaires/' % server
    url_localisation = 'http://%s/localisations/' % server

    # browse each map
    for mapcode in sorted(maps):
        for filename in maps[mapcode]:
            check_file(filename)
            filepath = '/'.join([directory, mapcode, filename])
            frag = read_frag(filepath)
            coordinate = filename.split('.')[0]
            map_url = '%s%s/' % (url_map, map_id)
            coordinate_url = get_or_create_coordinate_url(
                url_localisation,
                coordinate,
                map_url)
            if frag != {}:
                process(frag, coordinate_url, ressources, url_inventaire, url_ressources)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
