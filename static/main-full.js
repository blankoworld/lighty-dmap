(function () {
    "use strict";
    var map_type,
        map_gfx,
        maps,
        max_weighting,
        tiles_color = "24, 24, 255",
        tiles_html = "",
        best_maps,
        img_loaded = false,
        check_map_loader = null,
        preloadImg,
        positions = true,
        zoom = 0.8;

    function getJson() {
        var html = document.getElementById('dmap').innerHTML,
            json;

        html = html.replace(/<br>/g, "");
        html = html.replace('/[^A-Za-z0-9{}-,:"\'_]/g', "");
        json = JSON.parse(html);

        map_type = json.map_type;
        maps = json.maps;
        best_maps = json.max_weighting.maps;
        max_weighting = json.max_weighting.qte;
        document.getElementById('dmap').innerHTML = "<div class='loader'>Chargement en cours ...</div>";
    }

    function getConfigs() {
        map_gfx = {};
        map_gfx.maps = {};
        map_gfx.maps.amakna = {
            "size_x": (10000 * zoom),
            "size_y": (8000 * zoom),
            "tile_x_min": -93,
            "tile_x_max": 50,
            "tile_y_min": -99,
            "tile_y_max": 61};
        map_gfx.maps.incarnam = {
            "size_x": (2375 * zoom),
            "size_y": (1952 * zoom),
            "tile_x_min": -5,
            "tile_x_max": 8,
            "tile_y_min": -9,
            "tile_y_max": 9};
        map_gfx.current_map = map_gfx.maps[map_type];
        map_gfx.tile_size_x = map_gfx.current_map.size_x / (Math.abs(map_gfx.current_map.tile_x_min) + Math.abs(map_gfx.current_map.tile_x_max));
        map_gfx.tile_size_y = map_gfx.current_map.size_y / (Math.abs(map_gfx.current_map.tile_y_min) + Math.abs(map_gfx.current_map.tile_y_max));
    }

    function prepareTile(x, y, text, rank) {
        var style,
            html,
            offset_x,
            offset_y;

        if (rank < 0.3)
            rank = 0.3;

        offset_x = (Math.abs(map_gfx.current_map.tile_x_min) + x) * (map_gfx.tile_size_x);
        offset_y = (Math.abs(map_gfx.current_map.tile_y_min) + y) * (map_gfx.tile_size_y);
        style = 'height:' + Math.round(map_gfx.tile_size_y) + 'px;';
        style += 'width:' + Math.round(map_gfx.tile_size_x) + 'px;';
        style += 'left:' + Math.round(offset_x) + 'px;';
        style += 'top:' + Math.round(offset_y) + 'px;';
        style += 'background-color: rgba(' + tiles_color + ', ' + rank + ');';
        html = '<div class="map_tile" style="' + style + '">';
        if (positions && zoom >= 0.5) {
            html += '<div class="positions">' + x + '/' + y + '</div>';
        }
        html += '<div class="map_tile_tooltip">';
        html += '<div class="map_tile_title">' + x + '/' + y + '</div>';
        html += '<div class="map_tile_infos">' + text + '</div>';
        html += '</div>';
        html += '</div>';
        tiles_html += html;
    }

    function createCanvas() {
        var style,
            html,
            canvas;

        canvas = document.getElementById('dmap');
        style = 'height:' + map_gfx.current_map.size_y + 'px;';
        style += 'width:' + map_gfx.current_map.size_x + 'px;';
        style += 'background-image:url(../static/img/' + map_type + '_np.jpg);';
        html = '<div id="settings"></div>';
        html += '<div id="dmap_container">';
        html += '<div id="map_gfx" class="map_gfx" style="' + style + '"></div>';
        html += '</div>';

        canvas.innerHTML = html;
    }

    function autoScroll() {
        var scrollMap = best_maps[0], // Pseudo random !
            canvas,
            x,
            y,
            offset_x,
            offset_y;

        x = parseInt(scrollMap.split(",")[0], 10);
        y = parseInt(scrollMap.split(",")[1], 10);
        offset_x = (Math.abs(map_gfx.current_map.tile_x_min) + x) * (map_gfx.tile_size_x) - 500;
        offset_y = (Math.abs(map_gfx.current_map.tile_y_min) + y) * (map_gfx.tile_size_y) - 150;

        canvas = document.getElementById('dmap_container');
        canvas.scrollLeft = offset_x;
        canvas.scrollTop = offset_y;
    }

    function parseTiles() {
        var map,
            x,
            y,
            resources,
            map_resource,
            text;

        for (map in maps) {
            if (maps.hasOwnProperty(map)) {
                x = parseInt(map.split(",")[0], 10);
                y = parseInt(map.split(",")[1], 10);

                text = "";
                map_resource = 0;
                for (resources in maps[map]) {
                    if (maps[map].hasOwnProperty(resources)) {
                        map_resource += maps[map][resources].qte;
                        
                        if (maps[map][resources].safe !== undefined && maps[map][resources].safe === true) {
                            text += '<span class="verified">&clubs; </span>';
                        } else {
                            text += '<span class="unverified"></span>';
                        }
                        text += '<span class="qte">' + maps[map][resources].qte + '</span> ' +  resources;
                        if (maps[map][resources].hide !== undefined && maps[map][resources].hide !== 0) {
                            text += ' <span class="hide"> &dagger;</span>';
                        }
                        text += '<br />';
                    }
                }
                prepareTile(x, y, text, (map_resource / max_weighting));
            }
        }
    }

    function addEvents() {
        var canvas,
            curYPos = 0,
            curXPos = 0,
            curSrollX = 0,
            curSrollY = 0,
            curDown = false;

        canvas = document.getElementById('dmap_container');
        map_gfx.updating = false;
        canvas.addEventListener('mousemove', function (e) {
            if (curDown === true && map_gfx.updating === false) {
                map_gfx.updating = true;
                canvas.scrollLeft = curSrollX + curXPos - (e.pageX - e.currentTarget.offsetLeft);
                canvas.scrollTop = curSrollY + curYPos - (e.pageY - e.currentTarget.offsetTop);
                setTimeout(function(){ map_gfx.updating = false;}, 50);
            }
        });
        canvas.addEventListener('mousedown', function (e) {
            curDown = true;
            curSrollX = canvas.scrollLeft;
            curSrollY = canvas.scrollTop;
            curYPos = e.pageY - e.currentTarget.offsetTop;
            curXPos = e.pageX - e.currentTarget.offsetLeft;
        });
        canvas.addEventListener('mouseup', function (e) { curDown = false; });
        canvas.addEventListener('mousewheel', function (e) { switchZoom(e.wheelDelta) });
        canvas.addEventListener('DOMMouseScroll', function (e) { switchZoom(- e.detail) });
    }

    function switchZoom(delta) {
        var zoominput = document.getElementById("zoominput");

        if (delta < 0 && zoominput[zoominput.selectedIndex + 1] != undefined) {
            zoominput.value = zoominput[zoominput.selectedIndex + 1].value;
            changeZoom(zoominput.value);
        } else if (delta > 0 && zoominput[zoominput.selectedIndex - 1] != undefined) {
            zoominput.value = zoominput[zoominput.selectedIndex - 1].value;
            changeZoom(zoominput.value);
        }

    }

    function createSettings() {
        var html;

        html = '';
        html += '<div id="hide_settings" onclick="changeDisplaySettings()"></div>';
            html += '<div id="settings_content">';
                html += 'Ressources par cases : <br />';
                html += '<div>0 <span id="settings_waighting"></span> '+max_weighting+'</div>';
                html += '<br />';
                html += 'Changer de zoom :';
                    html += '<select id="zoominput" onchange="changeZoom(this.value)">';
                    html += '<option '+((zoom==1) ? 'selected="true"' : '') +' value="1">x 1</option>';
                    html += '<option '+((zoom==0.9) ? 'selected="true"' : '') +' value="0.9">x 0.9</option>';
                    html += '<option '+((zoom==0.8) ? 'selected="true"' : '') +' value="0.8">x 0.8</option>';
                    html += '<option '+((zoom==0.7) ? 'selected="true"' : '') +' value="0.7">x 0.7</option>';
                    html += '<option '+((zoom==0.6) ? 'selected="true"' : '') +' value="0.6">x 0.6</option>';
                    html += '<option '+((zoom==0.5) ? 'selected="true"' : '') +' value="0.5">x 0.5</option>';
                    html += '<option '+((zoom==0.4) ? 'selected="true"' : '') +' value="0.4">x 0.4</option>';
                    html += '<option '+((zoom==0.3) ? 'selected="true"' : '') +' value="0.3">x 0.3</option>';
                    html += '<option '+((zoom==0.2) ? 'selected="true"' : '') +' value="0.2">x 0.2</option>';
                html += '</select>';
                html += '<br />';
                html += '<br />';
                html += '&clubs; : Ressource vérifiée par le staff <br />';
                html += '<br />';
                html += '&dagger; : Ressource cachée (Grotte, mine,...)<br />';
                html += '<br />';
                html += 'Afficher les positions: <input type="checkbox" '+ ((positions) ? 'checked' : '') + ' onchange="changeDisplayPositions()" /><br />';
            html += '</div>';
            html += '<div id="clear"></div>';
        document.getElementById('settings').innerHTML = html;
    }

    function preloadMap() {
        preloadImg = new Image();
        preloadImg.onload = loadedMap;
        preloadImg.src = '../static/img/' + map_type + '_np.jpg';
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
        }
        return "";
    }

    /* Global function */
    window.loadedMap = function() {
	   img_loaded = true;
    }
    
    window.loadData = function () {
    	if (img_loaded === true) {
    	  img_loaded = false;
    	  createCanvas();
    	  autoScroll();
    	  createSettings();
    	  //Put tiles
    	  document.getElementById('map_gfx').innerHTML = tiles_html;
    	  addEvents();
    	  if (getCookie('settings') === "0") {
    	      changeDisplaySettings();
    	  }
    	  if (getCookie('menu') === "0") {
    	      changeDisplayMenu('hideMenu');
    	  }
    	} else {
            check_map_loader = setTimeout(function(){loadData()}, 200);
        }
    }

    window.changeDisplayList = function (element) {
        var ul = element.querySelector("ul");
        if (ul.style.display === "block") {
            ul.style.display = "none";
        } else {
            ul.style.display = "block";
        }
    };

    window.changeDisplayPositions = function () {
        var canvas,
            scrollLeft,
            scrollTop;

        positions = !positions;
        canvas = document.getElementById('dmap_container');
        scrollLeft = canvas.scrollLeft;
        scrollTop = canvas.scrollTop;
        document.cookie="positions="+positions+"; path=/";
        tiles_html = "";
        getConfigs();
        parseTiles();
        createCanvas();
        canvas = document.getElementById('dmap_container');
        canvas.scrollLeft = scrollLeft;
        canvas.scrollTop = scrollTop;
        //autoScroll();
        createSettings();
        document.getElementById('map_gfx').innerHTML = tiles_html;
        addEvents();
    };

    window.changeDisplayMenu = function (className) {
        document.getElementById('menu').className = className;
        if (className === "displayMenu") {
            document.cookie="menu=1; path=/";
        } else {
            document.cookie="menu=0; path=/";
        }
    };

    window.changeDisplaySettings = function () {
        var settings_content = document.getElementById("settings_content");
        var hide_settings = document.getElementById("hide_settings");
        var settings = document.getElementById("settings");
        if (settings_content.style.display === "none") {
            settings_content.style.display = "block";
            settings.style.width = "200px";
            hide_settings.style.backgroundImage = 'url("../static/img/Arrow-right.png")';
            document.cookie="settings=1; path=/";
        } else {
            settings_content.style.display = "none";
            settings.style.width = "20px";
            hide_settings.style.backgroundImage = 'url("../static/img/Arrow-left.png")';
            document.cookie="settings=0; path=/";
        }
    };

    window.changeZoom = function (tozoom) {
        var canvas,
            old_x,
            old_y,
            width,
            height,
            scrollLeft,
            scrollTop;

        canvas = document.getElementById('dmap_container');
        scrollLeft = canvas.scrollLeft;
        scrollTop = canvas.scrollTop;
        width = canvas.clientWidth;
        height = canvas.clientHeight;
        old_x = (scrollLeft + (width / 2)) / map_gfx.tile_size_x - Math.abs(map_gfx.current_map.tile_x_min);
        old_y = (scrollTop + (height / 2)) / map_gfx.tile_size_y - Math.abs(map_gfx.current_map.tile_y_min);
        document.cookie="zoom="+tozoom+"; path=/";
        zoom = tozoom;
        tiles_html = "";
        getConfigs();
        parseTiles();
        createCanvas();
        canvas = document.getElementById('dmap_container');
        canvas.scrollLeft = (Math.abs(map_gfx.current_map.tile_x_min) + old_x) * (map_gfx.tile_size_x) - 500;
        canvas.scrollTop = (Math.abs(map_gfx.current_map.tile_y_min) + old_y) * (map_gfx.tile_size_y) - 150;
        createSettings();
        document.getElementById('map_gfx').innerHTML = tiles_html;
        addEvents();
    };

    window.onload = function () {
        if (getCookie('positions') === "false") {
            positions = false;
        }
        if (getCookie('zoom') !== "") {
            zoom = getCookie('zoom');
        }
        getJson();
        getConfigs();
        preloadMap();
        parseTiles();
        if (false) {
            //AMAKNA
            prepareTile(-42, -18, "TESTMAP", 2); // Ile minotorror
            prepareTile(-42, -17, "TESTMAP", 2); // Ile minotorror
            prepareTile(-41, -18, "TESTMAP", 2); // Ile minotorror
            prepareTile(-41, -17, "TESTMAP", 2); // Ile minotorror
            prepareTile(-90, -90, "TESTMAP", 2); // Ile minotorror
        }
        if (false) {
            //INCARNOOB
            prepareTile(9, 2, "TESTMAP", 2); // Vide, nord ballon
        }
        check_map_loader = setTimeout(function(){loadData()}, 200);
    };
}());
