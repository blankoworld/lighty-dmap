# Mise à jour du 20 juillet 2016

  * La carte du monde d'Amakna a été mise à jour (elle présente le temple des alliances, le palais royal à Sufokia et Frigost³
  * suite à la mise à jour 2.35 de Dofus, nous ajoutons progressivement les ressources des tréfonds de Sufokia
  * Vulkania (l'île) est ouverte

Alors n'hésitez pas à [participer vous aussi](./apropos.html) pour remplir ces nouvelles zones !
