#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# defrag.py
#
# Read a directory in which you have some directory about some MAP.
# Each MAP directory contains its box (with coordinate).
# Each box is a file that is named a FRAG.
# Each box/FRAG contains some resources.
# Each box/FRAG is formated as an .ini file.

support = True
program_name = u'Lighty Dmap'
default_directory = 'test/frags'
extension = '.blk'
about_page = True
head = 'header.tmpl'
foot = 'footer.tmpl'
endfile = u'index.html'
static_dir = 'static'
index_title = u'Accueil'
about_title = u'À propos'
about_name = u'apropos'
result_directory = 'pub'
result_extension = u'.html'
content_start = u'<div id="dmap">'
content_stop = u'</div>'
authorized_resources = {
    u'anguille': u'Anguille',
    u'aquajou': u'Aquajou',
    u'argent': u'Argent',
    u'atelier': u'Atelier',
    u'avoine': u'Avoine',
    u'bambou': u'Bambou',
    u'bambou_sacre': u'Bambou Sacré',
    u'bambou_sombre': u'Bambou Sombre',
    u'banque': u'Banque',
    u'bar': u"Bar Rikain",
    u'bauxite': u'Bauxite',
    u'belladone': u'Belladone',
    u'ble': u'Blé',
    u'bombu': u'Bombu',
    u'brochet': u"Brochet",
    u'bronze': u'Bronze',
    u'cadeau': u'Paquet Cadeau',
    u'carpe': u"Carpe d'Iem",
    u'chanvre': u'Chanvre',
    u'charme': u'Charme',
    u'chataignier': u'Châtaignier',
    u'chaton': u"Poisson-Chaton",
    u'chene': u'Chêne',
    u'crabe': u"Crabe Sourimi",
    u'cuivre': u'Cuivre',
    u'dolomite': u'Dolomite',
    u'dorade': u"Dorade Grise",
    u'ebene': u'Ébène',
    u'ecume': u'Écume de mer',
    u'edelweiss': u'Edelweiss',
    u'egouts': u'Entrée vers les égouts',
    u'erable': u'Érable',
    u'espadon': u"Espadon",
    u'etain': u'Étain',
    u'fer': u'Fer',
    u'frene': u'Frêne',
    u'frostiz': u'Frostiz',
    u'ginseng': u'Ginseng',
    u'goujon': u"Goujon",
    u'greuvette': u"Greuvette",
    u'houblon': u'Houblon',
    u'if': u'If',
    u'kaliptus': u'Kaliptus',
    u'kobalte': u'Kobalte',
    u'kralamoure': u"Kralamoure",
    u'lin': u'Lin',
    u'lotte': u"Lotte",
    u'mais': u'Maïs',
    u'malt': u'Malt',
    u'mandragore': u'Mandragore',
    u'manganese': u'Manganèse',
    u'menthe': u'Menthe Sauvage',
    u'merisier': u'Merisier',
    u'millet': u'Millet',
    u'mine': u'Entrée de mine',
    u'morue': u"Morue",
    u'noisetier': u'Noisetier',
    u'noyer': u'Noyer',
    u'obsidienne': u'Obsidienne',
    u'oliviolet': u'Oliviolet',
    u'or': u'Or',
    u'orchidee': u'Orchidée Sauvage',
    u'orge': u'Orge',
    u'orme': u'Orme',
    u'ortie': u'Ortie',
    u'pandouille': u'Pandouille',
    u'pane': u"Poisson Pané",
    u'patelle': u'Patelle',
    u'perce-neige': u'Perce-Neige',
    u'perche': u"Perche",
    u'phenix': u'Statue Phénix',
    u'poisskaille': u"Poisskaille",
    u'poubelle': u'Poubelle',
    u'puit': u'Puit',
    u'quisnoa': u'Quisnoa',
    u'raie': u"Raie Bleue",
    u'requin': u"Requin Marteau-Faucille",
    u'riz': u'Riz',
    u'rp': u'Pièces pour JdR',
    u'salikrone': u'Salikrone',
    u'sardine': u"Sardine Brillante",
    u'sauge': u'Sauge',
    u'seigle': u'Seigle',
    u'silicate': u'Silicate',
    u'tanche': u"Tanche",
    u'trefles': u'Trèfles',
    u'tremble': u'Tremble',
    u'truite': u"Truite",
    u'zaap': u'Zaap',
    u'zaapi': u'Zaapi',
}
craft_groups = {
    u'Paysan': [u'lin', u'chanvre', u'ble', u'orge', u'avoine', u'houblon', u'seigle', u'riz', u'malt', u'frostiz', u'mais', u'millet', u'quisnoa'],
    u'Alchimiste': [u'ortie', u'sauge', u'menthe', u'trefles', u'orchidee', u'edelweiss', u'pandouille', u'perce-neige', u'ginseng', u'belladone', u'mandragore', u'salikrone'],
    u'Mineur': [u'fer', u'cuivre', u'bronze', u'kobalte', u'argent', u'bauxite', u'manganese', u'or', u'etain', u'silicate', u'dolomite', u'obsidienne', u'mine', u'ecume'],
    u'Bûcheron': [u'frene', u'chataignier', u'noyer', u'chene', u'bombu', u'erable', u'oliviolet', u'if', u'bambou', u'merisier', u'noisetier', u'ebene', u'kaliptus', u'charme', u'bambou_sombre', u'orme', u'bambou_sacre', u'tremble', u'aquajou'],
    u'Pêcheur': [u'goujon', u'greuvette', u'truite', u'crabe', u'chaton', u'pane', u'carpe', u'sardine', u'brochet', u'kralamoure', u'anguille', u'dorade', u'perche', u'raie', u'lotte', u'requin', u'bar', u'morue', u'tanche', u'espadon', u'poisskaille', u'patelle'],
    u'Divers': [u'poubelle', u'puit', u'zaap', u'zaapi', u'banque', u'rp', u'cadeau', u'atelier', u'phenix', u'egouts'],
}

craft_names = {
    u'Paysan': u'paysan',
    u'Alchimiste': u'alchimiste',
    u'Mineur': u'mineur',
    u'Bûcheron': u'bucheron',
    u'Pêcheur': u'pecheur',
    u'Divers': u'divers',
}

from string import Template
import sys
from os.path import exists
from os.path import isdir
from os import listdir
from os import mkdir
import re
from json import dumps
from shutil import copytree, rmtree
from random import randint

py = sys.version_info
py3k = py >= (3, 0, 0)
if py3k:
    from configparser import ConfigParser
    from configparser import DuplicateSectionError
else:
    from ConfigParser import SafeConfigParser as ConfigParser

# Regular expression for the filename (resources)
#+ Is composed of:
#+  - a minus char that appears 0 or 1
#+  - some digit that appear 1 or 3 digits
#+  - a comma (mandatory)
#+  - a minus char (as previously, 0 or 1 time)
#+  - some digit that appear 1 or 3 digits
#+  - the end (so more than 3 digits is not allowed
regex_filename = re.compile('(-?\d{1,3},-?\d{1,3})%s$' % extension)

def get_filename(subdir, name):
    """
    Return real filename from the public directory to the HTML file wanted.
    The filename is composed of:
      - result directory
      - subdir
      - name
      - extension
    """
    res = '/'.join([result_directory, subdir, name])
    res += result_extension
    return res

def get_filecontent(page):
    """
    Read the file and give the result.
    """
    result = ''
    if not page:
        return result
    with open(page, 'r') as f:
        result = f.read()
        f.close()
    return result.decode('utf-8')

def check_directory(dirname):
    """
    Check that directory exists.
    That some directory are into. Each directory is a MAP.
    And that some file are into each MAP.
    """
    # Some checks
    if not dirname:
        raise Exception("Please give a directory.")
        return False
    if not exists(dirname):
        raise Exception("This directory doesn't exist: %s" % dirname)
    # Prepare some values
    maps = {}
    useful_files = False
    for element in listdir(dirname):
        element_path = '/'.join([dirname, element])
        if not isdir(element_path):
            print("[EXCLUDED] Not a directory: %s" % element_path)
            continue
        files = listdir(element_path)
        if not files:
            print("[EXCLUDED] No file found: %s" % element_path)
            continue
        maps.update({element.lower(): files})
        for f in files:
            # Check that there is at least 1 file that is readable
            if f.endswith(extension):
                useful_files = True
                break
    if not useful_files:
        raise Exception("No %s files found!" % extension)
    return maps

def read_frag(filename):
    """
    Read file and extract only authorized resources.
    """
    # Prepare some values
    res = {}
    filepath = '/'.join([directory, filename])
    # Read file
    conf = ConfigParser()
    try:
        conf.read(filepath)
    except DuplicateSectionError as e:
        print('Duplicate: %s' % filepath)
        return res
    for section in conf.sections():
        section_name = section.lower()
        # Do not process non authorized resources
        if section_name not in authorized_resources.keys():
            print("\t%s: unknown" % section_name)
            continue
        if section not in res:
            res[section_name] = {}
        # By default consider that resources were not checked/verified (safe = False)
        res[section_name].update({
            'safe': False,
        })
        for key, value in conf.items(section):
            # Make safe as a boolean
            if key == 'safe' and value != 0:
                value = True
            # Make qte to be an integer
            if key == 'qte':
                value = int(value)
            # Make hide as integer only if superior to 0
            if key == 'hide' and int(value) > 0:
                value = int(value)
            # write result
            res[section_name].update({
                key: value,
            })
    return res

def extract_frags(map_directory, files):
    """
    Read each file and extract the frag (a box on the map).
    """
    res = {}
    # Browse file
    for item in files:
        # Do not process files that have not the right extension
        if not item.endswith(extension):
            print("[EXCLUDED]  %s (wrong extension)" % item)
            continue
        # Do not process malformed files
        if not regex_filename.search(item):
            print("[MALFORMED] %s (wrong name)" % item)
            continue
        print("Processing %s…" % item)
        match = regex_filename.match(item)
        coordinate = match.groups()[0]
        filepath = '/'.join([map_directory, item])
        frag_dict = read_frag(filepath)
        res.update({
            coordinate: frag_dict,
        })
    return res

def extract_resource(map_name, frags, resources=[]):
    """
    Check the entire frags dict to extract given 'resources'.
    If no resources given, just change all resources code by their name (in 
    authorized_resources).
    """
    # Prepare some values
    maximum = None
    res = {
        'map_type': map_name,
        'max_weighting': {},
        'maps': {},
    }
    # Give to JSON the current map in which we are (this is for #active menu)
    res['current'] = ''
    if not resources:
        res['current'] = map_name
    elif len(resources) == 1:
        res['current'] = resources[0]
    else:
        for group in craft_groups:
            if craft_groups[group] == resources:
                res['current'] = craft_names[group]
                break

    def frag_initialization(f):
        """
        Initialize the coordinate in the dict.
        """
        if f not in res['maps']:
            res['maps'][f] = {}
        return True

    def add_resource(f, r):
        """
        Add resource values in the dict.
        """
        res['maps'][f].update({
            authorized_resources[r]: frags[f].get(r),
        })
        return True

    def qty_max(coordinate, name=False):
        """
        Check dict regarding coordinate and process all quantities to have the max.
        Return the max found for this coordinate.
        """
        # First check the maximum of the frag regarding all ressources (name is False) or just one resource
        qty = 0
        if not name:
            for element in frags[coordinate]:
                qty += frags[coordinate][element].get('qte', 0)
        else:
            if name in frags[coordinate]:
                qty += frags[coordinate][name].get('qte', 0)
        return qty

    def update_max(coordinate, max_found, maximum):
        """
        Compare global maximum of resource to the given maximum found.
        """
        # Compare quantity with the maximum
        if maximum is None and max_found > 0:
            maximum = max_found
            res['max_weighting'] = {
                'qte': max_found,
                'maps': [coordinate],
            }
        elif maximum == max_found:
            qty_maps = res['max_weighting'].get('maps')
            qty_maps.append(coordinate)
            res['max_weighting'].update({
                'maps': qty_maps,
            })
        elif max_found > maximum:
            maximum = max_found
            res['max_weighting'].update({
                'qte': max_found,
                'maps': [coordinate],
            })
        return maximum

    for frag in frags:
        frag_max = 0
        # Add each resource from frag if no resources given
        if not resources:
            frag_initialization(frag)
            for resource_code in frags[frag]:
                add_resource(frag, resource_code)
                frag_max = qty_max(frag)
        # Otherwise add only resource
        else:
            for resource in resources:
                if resource in frags[frag]:
                    frag_initialization(frag)
                    add_resource(frag, resource)
                    frag_max = qty_max(frag, resource)
        # For frag_max found, update result
        if frag_max:
            maximum = update_max(frag, frag_max, maximum)
        # Delete frag from result if empty
        if frag in res['maps'] and not res['maps'][frag]:
            res['maps'].pop(frag)
    return res

def webpage_prettify(stream):
    """
    Prettify stream so that it would be a HTML5 indented code.
    """
    # FIXME: find a way to do this to keep the DOCTYPE and keep accentued chars!
#    from lxml import etree, html
#    root = html.fromstring(stream)
#    res = etree.tostring(root, encoding='utf-8', pretty_print=True)
#    return res
    return stream

def read_markdown_file(filename):
    """
    Read the content of the given filename and generate an HTML output using Markdown
    """
    res = u''
    # Open the given file
    with open(filename, 'r') as givenfile:
        content = givenfile.read()
        givenfile.close()
    # Prepare HTML content
    try:
        mdwn = __import__('markdown')
        res += mdwn.markdown(content.decode('utf-8'))
    except ImportError as e:
        res = 'python-markdown module missing!'
    return res

def generate_craft_menu():
    """
    Generate a list of resources sorted by craft.
    """
    res = ''
    for group in sorted(craft_groups):
        res += u"""
                <li id="%s" onclick="changeDisplayList(this)">+ <a href="%s%s" title="%s">%s</a>
                <ul>""" % (craft_names[group], craft_names[group], result_extension, group, group)
        for resource in sorted(craft_groups[group]):
            res += u"""
                    <li id="%s"><a href="%s%s" title="%s">%s</a></li>""" % (resource, \
                        resource, result_extension, authorized_resources[resource], \
                        authorized_resources[resource])
        res += u"""
                </ul>
            </li>"""
    return res

def generate_menu(title=False, craft=False, hidemenu=True):
    """
    Generate a menu regarding group of craft and resources
    """
    # Prepare some values
    class_menu = 'class="hideMenu"'
    if not hidemenu:
        class_menu = ''
    res = u"""
<div id="menu" %s>
    <div id="menuContent">
        <div id="menuButtonClose" onclick="changeDisplayMenu('hideMenu')"></div>
        <div id="menuTitle"><img src="${menu_depth}/static/img/dofus.png" alt="LOGO"/><br />%s</div>
        <nav id="nav">
            <ul>
                <li><a href="${menu_depth}index%s" title="%s">%s</a></li>""" % (class_menu, program_name, result_extension, index_title, index_title)
    if craft:
        if title:
            res += """
                <li id="%s"><a href="index%s" title="%s">%s</a></li>""" % (title.lower(), result_extension, title, title)
        res += craft_menu
    if about_page:
        res += u"""
                <li id="%s"><a href="${menu_depth}%s%s" title="%s">%s</a></li>""" % (about_name, about_name, result_extension, about_title, about_title)
    res += u"""
            </ul>
        </nav>
        <div id="menuFooter">2016 - Lighty-Dmap <br /> Toutes les illustrations sont la propriété d'Ankama.</div>
    </div>
</div>
<div id="menuButtonOpen" onclick="changeDisplayMenu('displayMenu')"></div>\n\n"""
    return res

def generate_about(filename):
    """
    Generate a webpage with all resources given.
    Frags is a dict that contains all frags of a given map.
    """
    # Prepare some values
    res = u'<div id="about">'
    res += read_markdown_file('about.md')
    res += u'</div>'
    # Generate the webpage
    generate_webpage(filename, about_title, res, depth=0, menu=True, craft=False, hidemenu=True)
    return True

def generate_webpage(filename, title, content, depth=False, menu=False, menu_title=False, craft=False, hidemenu=False):
    """
    Create a page with header, content, then footer.
    Replace some string into:
     - webpagetitle: by the program name
     - title: by the given title
    Depth is an integer that give the depth of your webpage in the tree.
    For an example, if your page is /dir1/other_dir/index.html, give 2 as depth and it will make ../../.
    """
    # Make template
    relative_path = './'
    menu_depth = './'
    if depth:
        relative_path = '../'*depth
        menu_depth = '../'*depth
    future_template = header + content
    if menu:
        future_template += generate_menu(menu_title, craft, hidemenu)
    future_template += footer
    tmpl_content = Template(future_template)
    replaced_content = tmpl_content.safe_substitute({
        'webpagetitle': program_name + ' - ' + title,
        'title': title,
        'path': relative_path,
        'menu_depth': menu_depth,
    })
    # Prettify result
    res = webpage_prettify(replaced_content)
    # Write result into given filename
    with open(filename, 'w') as final:
        final.write(res.encode('utf-8'))
        final.close()
    return True

def dict2html(dictionnary):
    """
    Transform a dictionnary to a JSON table so that it can be included in a HTML web page.
    Output: JSON table in a HTML tag.
    """
    # Result transformation to JSON
    json_res = dumps(dictionnary, sort_keys=True, indent=True, separators=(',', ': '))
    # Add <br/> after each end of line in JSON
    content = json_res.replace('\n', '<br />\n')
    # Make content with HTML start tag and en tag
    res = '\n'.join([content_start, content, content_stop])
    res += '\n' # Fix </body> to begin a new line
    return res

def generate_map_index(map_name, frags):
    """
    Generate a webpage with all resources given.
    Frags is a dict that contains all frags of a given map.
    """
    # Prepare some values
    new_dict = extract_resource(map_name, frags, False)
    # Prepare HTML content
    res = dict2html(new_dict)
    # Prepare name for the webpage
    name = get_filename(map_name, 'index')
    # Generate the webpage
    generate_webpage(name, map_name.title(), res, depth=1, menu=True, craft=True)
    return True

def generate_single(map_name, frags, resources, name, title='noname'):
    """
    Browse frags and take given resources from.
    Prepare result as JSON and make the HTML result.
    """
    # Some verifications
    if not isinstance(resources, list):
        resources = [resources]
    # Extract only given resource from frags
    resource_dict = extract_resource(map_name, frags, resources)
    # Prepare content
    res = dict2html(resource_dict)
    # Prepare elements for webpage
    webname = get_filename(map_name, name)
    # Generate the webpage
    generate_webpage(webname, title, res, depth=1, menu=True, menu_title=map_name.title(), craft=True)
    return True

def generate_resources(map_name, frags):
    """
    For each resource generate a single resource html page.
    """
    for resource in authorized_resources:
        generate_single(map_name, frags, [resource], resource, authorized_resources.get(resource, resource))
    return True

def generate_craft_page(map_name, frags):
    """
    For each craft group, generate a page containing linked resources.
    """
    for craft in craft_groups:
        generate_single(map_name, frags, craft_groups[craft], craft_names[craft], craft)
    return True

def generate_checked_frags(map_name, frags):
    """
    Generate a page in which all checked maps where.
    If file exist, it have a weight of 1.
    """
    checked_dict = {
        'current': map_name,
        'map_type': map_name,
        'max_weighting': {},
        'maps': {},
    }
    if frags:
        max_checked = len(frags)
        first_map_nb = randint(0, max_checked - 1)
        checked_dict.update({
            'max_weighting': {
                'maps': [frags.keys()[first_map_nb]],
                'qte': 1,
            },
        })
    for frag in frags:
        checked_dict['maps'][frag] = {
            'Fichier existant': {
                'qte': 1,
            },
        }
    res = dict2html(checked_dict)
    name = get_filename(map_name, u'dejafait')
    title = u'Déjà faites'
    generate_webpage(name, title, res, depth=1, menu=False, menu_title=map_name.title(), craft=True)
    return True

def generate_tocheck_frags(map_name, frags):
    """
    Browse all frags. If one frag have one unchecked resource, so add it to the result.
    Then generate a webpage with it.
    """
    tocheck_dict = {
        'current': map_name,
        'map_type': map_name,
        'max_weighting': {},
        'maps': {},
    }
    tocheck_frag = []
    for frag in frags:
        for resource in frags[frag]:
            if 'safe' in frags[frag][resource] and frags[frag][resource]['safe'] is not True:
                if frag not in tocheck_dict['maps']:
                    tocheck_dict['maps'][frag] = {}
                tocheck_dict['maps'][frag].update({
                    resource: {
                        'qte': frags[frag][resource]['qte'] or 1,
                    }
                })
                tocheck_frag.append(frag)
    if tocheck_frag:
        max_tocheck = len(tocheck_frag)
        first_map_nb = randint(0, max_tocheck - 1)
        tocheck_dict.update({
            'max_weighting': {
                'maps': [tocheck_frag[first_map_nb]],
                'qte': 1,
            },
        })
    res = dict2html(tocheck_dict)
    name = get_filename(map_name, u'averifier')
    title = u'À vérifier'
    generate_webpage(name, title, res, depth=1, menu=False, menu_title=map_name.title(), craft=True)
    return True

def main(args=False):
    """
    Read a directory in which you find some square/box that contains which resources.
    First make a dict with all these informations.
    Then create a HTML page with all these result (1 page per resource).
    Finally make a HTML page with the entire result.
    """
    # Prepare some values
    global header
    header = get_filecontent(head)
    global footer
    footer = get_filecontent(foot)
    global directory
    directory = (args and len(args) > 1 and args[1]) or default_directory

    # Check source directory
    maps = check_directory(directory)

    if not maps:
        raise Exception("A problem occurs during: Read of files into %s" % directory)

    # Check result directory
    if not exists(result_directory):
        mkdir(result_directory)

    # Generate main menu
    global craft_menu
    craft_menu = generate_craft_menu()

    # Generate about's page
    about_filename = about_name + result_extension
    about_filepath = '/'.join([result_directory, about_filename])
    generate_about(about_filepath)

    # Browse each MAP
    html_content = ''
    for name in sorted(maps):
        # extract all frags
        frags = extract_frags(name, maps[name])
        # make map directory
        mappath = '/'.join([result_directory, name])
        if not exists(mappath):
            mkdir(mappath)
        # generate resources single pages
        generate_resources(name, frags)
        # generate craft pages
        generate_craft_page(name, frags)
        # generate an index page on which you have all links to all known resources single page + all resources page
        generate_map_index(name, frags)
        # Make the link to the map
        map_webpage = './' + name + '/' + 'index' + result_extension
        html_content += '\n\t\t<li><a href="%s" title="%s" class="frame"><span class="element">%s<br /><img src="./static/img/%s_symbol.jpg" alt=" " title="Symbole de %s" /></span></a></li>' % (map_webpage, name.title(), name.title(), name.title().lower(), name.title())
        # generate some useful maps for SUPPORT
        if support:
            generate_checked_frags(name, frags)
            generate_tocheck_frags(name, frags)

    # Make homepage content with a link to each MAP
    content = ''
    introduction_filename = 'intro.md'
    intro = u''
    if exists(introduction_filename):
        intro = read_markdown_file(introduction_filename)
    maj_filename = 'maj.md'
    maj = u''
    if exists(maj_filename):
        maj = read_markdown_file(maj_filename)
    if maj:
        content += u"""
<section id="maj">
    %s
</section>\n""" % (maj)
    if html_content:
        content += u"""
<section id="homepage">
    <ul id="maps">%s
    </ul>
</section>
<div class="space">&nbsp;</div>\n""" % html_content
    if intro:
        content += u"""
<section id="introduction">
    %s
</section>\n\n""" % intro
    html_endfile = '/'.join([result_directory, endfile])
    generate_webpage(html_endfile, program_name, content, menu=True, craft=False, hidemenu=True)

    # Copy static directory
    result_static_dir = '/'.join([result_directory, static_dir])
    if exists(result_static_dir):
        rmtree(result_static_dir)
    copytree(static_dir, result_static_dir)

    # end
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
