# Presentation

Lighty Dmap is a tool that generates a website with a Dofus Resource Map.

# Requirements

## Package

  * python
  * python-markdown

## Files

Some files in *frags* directory.

Each file should:

  * have a **.blk** extension
  * its name should be the coordinate of the map. For an example: **-10,50.blk**
  * contains some resources like this:```[Lin]
qte = 3
safe = 1
hide = 2```

If no safe or safe is 0, then it will just make a default safe is False.

**hide** parameter gives the number of resources that are hidden. Hidden resources are included into **qte** parameters.

# Use

Just launch it:

```
python defrag.py [directory]
```

**directory** parameter is optional.

It will give you all .html files for a webiste.

Each MAP have a kind of JSON table that will be used by JS code that compute it to render the MAP.

# JSON table

Here is the content of the JSON table: 

```
{
  "current": "amakna",
  "map_type": "amakna",
  "map_weighting": {
    "maps": [
      "-1,2"
    ]
    "qte": 2
  },
  "maps": {
    "-1,2": {
      "Bois": {
        "qte": 2,
        "safe": true,
      }
    },
    "-10,-50": {}
  }
}
```

# Map generation

In Dofus, maps are located in this directory: **Dofus/share/content/gfx/maps**.

Maps were exploded into a lot of pictures. To have a real map, we need to take all pictures and make one picture.

To do that we will:

  * copy the maps directory into test one
  * search how many pictures per line should be used for each map
  * generate a map
  * check it
  * redo the map twice so that we have a ligthweight map and a new one with better quality

As example are more verbose than a long explanation, have a look to the initial Amakna Dofus Map.

## Updated map

To have an updated map, you need to extract files from .d2p files.

So you need an extractor. We choose PyDofus (Cf. https://github.com/LuaxY/PyDofus).

To extract files, you need to:

  * clone the PyDofus repository
  * place all *.d2p files into **input** directory (from PyDofus)
  * go to PyDofus directory
  * launch this command: ```python d2p_unpack.py```
  * all files are in **output** directory
  * you need to copy some files to complete amakna's map, for an example copy worldmap0_1.d2p directory into wordlmap0.d2p one

Then you can use next script that creates a unique Amakna's map.

## Amakna

Amakna map is located in **Dofus/share/content/gfx/maps/1**.

We copy content of maps into test directory:

```
cd test
mkdir dmap
cp -r ~/Dofus/share/content/gfx/maps/* ./dmap/
```

Then we check the maps/1 directory and look after pictures. It seems that we have 20 or 30 maps. I find the right number: 40. To test if we are right, we launch this command:

```
./generate_maps.bash dmap/1/1 40 amakna-test.jpg 100
```

It will take a while, then you find the amakna-test.jpg result.

Note that 40 is the number of picture per line and 100 is the quality compression. 100 means no compression in fact (very good quality).

If it seems good, we do this:

```
./generate_maps.bash dmap/1/1 40 amakna-fast.jpg 1
```

This will make a very low quality picture, but useful to display quickly a kind of map during the load of another.

Then we make another map with low quality, but a good visual detail:

```
./generate_maps.bash dmap/1/1 40 amakna.jpg 10
```

It will generate a picture with 10% quality.

Copy the result in static directory:

```
cp amakna.jpg ../static/img/
cp amakna-fast.jpg ../static/img
```

That's all folks!
