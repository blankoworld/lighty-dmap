#!/bin/bash
#
# script-jpegtran.sh
#
# found here: http://orangina-rouge.org/shaarli/?YACP1Q
#
# REQUIREMENTS (on linux): apt-get install libjpeg-progs (Debin / Ubuntu)

in_path=$1
out_path=$2

for i in $in_path/*; do
  out_file=$(basename $i)
  jpegtran -optimize -outfile $out_path/$out_file $i
  jpegtran -progressive -outfile $out_path/$out_file $i
  jpegtran -grayscale -outfile $out_path/$out_file $i
done
