#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# generate_frags.py
#
# Generate files into given directory

x_min = -93
x_max = 50
y_min = -99
y_max = 61
max_resource = 10
max_qty_resource = 15
percentage_resource = 10 # Chance that you get a resource
directory = 'frageneration/amakna'
extension = '.blk'
authorized_resources = {
    u'lin': u'Lin',
    u'chanvre': u'Chanvre',
    u'trefles': u'Trèfles',
    u'menthe': u'Menthe Sauvage',
    u'orchidee': u'Orchidée Sauvage',
    u'edelweiss': u'Edelweiss',
    u'pandouille': u'Pandouille',
    u'perce-neige': u'Perce-Neige',
    u'petit_riviere': u'Petit poisson de rivière',
    u'petit_mer': u'Petit poisson de mer',
    u'riviere': u'Poisson de rivière',
    u'mer': u'Poisson de mer',
    u'gros_riviere': u'Gros poisson de rivière',
    u'gros_mer': u'Gros poisson de mer',
    u'geant_riviere': u'Poisson géant de rivière',
    u'geant_mer': u'Poisson géant de mer',
    u'ble': u'Blé',
    u'orge': u'Orge',
    u'avoine': u'Avoine',
    u'houblon': u'Houblon',
    u'seigle': u'Seigle',
    u'riz': u'Riz',
    u'malt': u'Malt',
    u'frostiz': u'Frostiz',
    u'fer': u'Fer',
    u'cuivre': u'Cuivre',
    u'bronze': u'Bronze',
    u'kobalte': u'Kobalte',
    u'manganese': u'Manganèse',
    u'etain': u'Étain',
    u'argent': u'Argent',
    u'silicate': u'Silicate',
    u'bauxite': u'Bauxite',
    u'or': u'Or',
    u'dolomite': u'Dolomite',
    u'obsidienne': u'Obsidienne',
    u'frene': u'Frêne',
    u'chataignier': u'Châtaignier',
    u'noyer': u'Noyer',
    u'chene': u'Chêne',
    u'oliviolet': u'Oliviolet',
    u'bombu': u'Bombu',
    u'erable': u'Érable',
    u'if': u'If',
    u'bambou': u'Bambou',
    u'merisier': u'Merisier',
    u'ebene': u'Ébène',
    u'kalyptus': u'Kalyptus',
    u'charme': u'Charme',
    u'bambou_sombre': u'Bambou Sombre',
    u'orme': u'Orme',
    u'bambou_sacre': u'Bambou Sacré',
    u'tremble': u'Tremble',
    u'poubelle': u'Poubelle',
    u'puit': u'Puit',
    u'zaap': u'Zaap',
    u'zaapi': u'Zaapi',
    u'banque': u'Banque',
    u'rp': u'Pièces pour JdR',
    u'mine': u'Entrée de mine',
    u'cadeau': u'Paquet Cadeau',
    u'atelier': u'Atelier',
    u'phenix': u'Statue Phénix',
}
craft_groups = {
    u'Paysan': [u'lin', u'chanvre', u'ble', u'orge', u'avoine', u'houblon', u'seigle', u'riz', u'malt', u'frostiz'],
    u'Alchimiste': [u'lin', u'chanvre', u'menthe', u'trefles', u'orchidee', u'edelweiss', u'pandouille', u'perce-neige'],
    u'Mineur': [u'fer', u'cuivre', u'bronze', u'kobalte', u'manganese', u'etain', u'argent', u'silicate', u'bauxite', u'or', u'dolomite', u'obsidienne'],
    u'Bûcheron': [u'frene', u'chataignier', u'noyer', u'chene', u'oliviolet', u'bombu', u'erable', u'if', u'bambou', u'merisier', u'ebene', u'kalyptus', u'charme', u'bambou_sombre', u'orme', u'bambou_sacre', u'tremble'],
    u'Pêcheur': [u'petit_riviere', u'petit_mer', u'riviere', u'mer', u'gros_riviere', u'gros_mer', u'geant_riviere', u'geant_mer'],
    u'Divers': [u'poubelle', u'puit', u'zaap', u'zaapi', u'banque', u'rp', u'mine', u'cadeau', u'atelier', u'phenix'],
}

import sys
from random import randint, getrandbits

py = sys.version_info
py3k = py >= (3, 0, 0)
if py3k:
    from configparser import ConfigParser
else:
    from ConfigParser import SafeConfigParser as ConfigParser

def main():
    """
    Check directory then generate files.
    """
    # Some checks
    if percentage_resource > 100:
        raise Exception('Error', 'Percentage superior to 100 is not authorized!')
    # Prepare some values
    code_resources = authorized_resources.keys()
    # Create a file for each frag in the map
    for x in range(x_min, x_max):
        for y in range(y_min, y_max):
            # Check if we have resource on this frag or not regarding chance
            draft = randint(1, 100)
            if draft <= (100 - percentage_resource):
                continue
            # Prepare some values
            how_many_resource = randint(1, max_resource)
            # Create frag filename
            filename = '%s,%s%s' % (x, y, extension)
            # Initialize the Frag that would be used to write in the file
            Frag = ConfigParser()
            # Get each resource of authorized_resources
            many_resource = 0
            resources = []
            while many_resource < how_many_resource:
                # Find a resource to display
                resource_nb = randint(1, len(authorized_resources))
                while resource_nb in resources:
                    resource_nb = randint(1, len(authorized_resources))
                resource = code_resources[resource_nb - 1]
                # Remember it
                resources.append(resource_nb)
                # Random resource quantity
                quantity = randint(1, max_qty_resource)
                # Add sections/attributes
                Frag.add_section(resource)
                Frag.set(resource, 'qte', str(quantity))
                # Tell system that we finish this resource
                many_resource += 1
            # Write result
            filepath = '/'.join([directory, filename])
            with open(filepath, 'w') as f:
                Frag.write(f)
                f.close()

    # end
    return 0

if __name__ == '__main__':
    sys.exit(main())

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
