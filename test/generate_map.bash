#!/usr/bin/env bash
#
# generate_map.bash
#
# Script that use a directory to create a MAP using some elements of it

#####
## TESTS
###

if test $# -ne 4
then
  echo "programme dossier profondeur sortie qualite"
  exit 1
fi

dossier=$1
profondeur=$2
sortie=$3
qualite=$4

if ! test -d ${dossier}
then
  echo "Dossier '${dossier}' non trouvé."
  exit 1
fi

if test -f ${sortie}
then
  echo "Fichier '${sortie}' existe déjà !"
  exit 1
fi

current=`pwd`
cd ${dossier} && montage `ls -1 *.jpg|sort -n` -tile ${profondeur}x -geometry +0+0 -strip -interlace Plane -quality ${qualite} ${current}/${sortie} && cd ${current}
