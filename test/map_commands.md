# Liste of Maps commands to generate Dofus Map

## Incarnam

### 0.25

```
./generate_map.bash dmap/2/0.25 5 incarnam.jpg 10
```

### 1

```
./generate_map.bash dmap/2/1 20 incarnam.jpg 10
```

## Amakna

### 0.75

```
./generate_map.bash dmap/1/0.75 15 amakna.jpg 10
```

### 1

```
./generate_map.bash dmap/1/1 40 amakna.jpg 10
```

## Souterrains d'Astrub

### 1

```
./generate_map.bash dmap/3/1 4 souterrains.jpg 50
```

## Labyrinthe du Minotoror

### 1

```
./generate_map.bash dmap/4/1 12 labyrinthe.jpg 10
```

## Maître Corbac

### 1

```
./generate_map.bash dmap/6/1 9 corbac.jpg 10
```

## Caverne Givrefoux

### 1

```
./generate_map.bash dmap/7/1 8 givrefoux.jpg 10
```

## Égouts de Bonta

### 1

```
./generate_map.bash dmap/8/1 13 egouts_bonta.jpg 10
```

## Égouts de Brâkmar

### 1

```
./generate_map.bash dmap/9/1 12 egouts_brakmar.jpg 10
```

## Village d'Otomaï

### 1

```
./generate_map.bash dmap/10/1 15 otomai.jpg 10
```
